#!/bin/bash
# How many times to try to connect to the DB before failing
check_attempts="${CHECK_ATTEMPTS:-3}"
# How many seconds to sleep between retries
sleep_between_checks="${SLEEP_BETWEEN_CHECKS:-3}"
# python executable
py_exec="${PY_EXEC:-python3}"
# Base direction with manage.py file
managepy_dir="${MANAGEPY_DIR:-.}"

for attempt_num in $(seq "$check_attempts"); do
  echo "Checking DB connection. ${attempt_num} attempt.";
  if "${py_exec}" "${managepy_dir}/manage.py" check --database default; then break; fi;
  fail="$?";
  sleep "${sleep_between_checks}";
done

if [ ! ${fail} ]; then
  "${py_exec}" "${managepy_dir}/manage.py" migrate;
  "${py_exec}" "${managepy_dir}/manage.py" collectstatic --noinput;
  "${py_exec}" "${managepy_dir}/manage.py" compilemessages;
  gunicorn core.wsgi:application -b "0.0.0.0:$PORT" -w "$WORKERS_COUNT" --threads "$THREADS_COUNT";
else
  echo "Server was not started, since no connection to DB was found.";
  exit 1;
fi
