from typing import Any
from urllib.parse import urljoin

import requests
from django.conf import settings
from requests import HTTPError, Timeout
from tenacity import retry, stop_after_attempt, retry_if_exception_type

from notifications.models import Client


class NotSentError(Exception):
    pass


class NotificationProvider:
    BASE_URL = "https://probe.fbrq.cloud/v1/"

    def send_message(self, client: Client, text: str) -> None:
        data = {
            "id": client.id,
            "phone": client.phone_number,
            "text": text
        }
        response = self._get_response("post", f"send/{client.id}", json=data, headers=self._base_headers)
        if response.get("message") != "OK":
            raise NotSentError

    @property
    def _base_headers(self):
        return {
            "Authorization": f"Bearer {settings.NOTIFICATION_TOKEN}",
            'Content-type': 'application/json',
        }

    @retry(
        retry=retry_if_exception_type((HTTPError, Timeout)),
        reraise=True,
        stop=stop_after_attempt(2),
    )
    def _get_response(
        self,
        method: str,
        path: str,
        *,
        json: dict[str, Any] | None = None,
        params: dict[str, Any] | None = None,
        headers: dict[str, str] | None = None,
        timeout: int = 5,
    ) -> dict[str, Any]:
        response = requests.request(
            method, self._get_full_url(path), json=json, params=params, headers=headers, timeout=timeout
        )

        response.raise_for_status()

        return response.json()


    def _get_full_url(self, path: str) -> str:
        return urljoin(self.BASE_URL, path)
