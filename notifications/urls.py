from django.urls import path, include
from rest_framework import routers

from notifications.views import ClientViewSet, DistributionViewSet

router = routers.SimpleRouter()
router.register(r"clients", ClientViewSet, basename="client")
router.register(r"distributions", DistributionViewSet, basename="distribution")

urlpatterns = [
    path("", include(router.urls))
]
