import logging

from celery import shared_task
from django.conf import settings

from notifications import services


logger = logging.getLogger("notifications.tasks")


@shared_task(name=settings.DISTRIBUTION_TASK_NAME)
def distribute_tokens(distribution_id: int) -> None:
    logger.info("notification.tasks.distribute_tokens started")

    services.distribute_tokens(distribution_id)

    logger.info("notification.tasks.distribute_tokens finished")
