import json
import logging
import re

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_celery_beat.models import IntervalSchedule, PeriodicTask


logger = logging.getLogger("notifications.models")


class Distribution(models.Model):
    start_at = models.DateTimeField()
    text = models.CharField(max_length=1023)
    filter = models.CharField(max_length=255)
    finish_at = models.DateTimeField()

    task = models.OneToOneField(PeriodicTask, on_delete=models.SET_NULL, blank=True, null=True)

    @property
    def filtered_clients_by_tag_or_operator_code(self):
        return Client.objects.filter(models.Q(tag=self.filter) | models.Q(operator_code=self.filter))

    def create_messages(self) -> None:
        """
        Messages are created for further distribution
        """
        Message.objects.bulk_create([
            Message(client=client, distribution=self)
            for client in self.filtered_clients_by_tag_or_operator_code
        ])

    def start_distribution(self) -> None:
        """
        Periodic task is created.
        First, all messages that will have to be sent are created, and then the task itself
        """
        self.create_messages()

        interval, _ = IntervalSchedule.objects.get_or_create(
            every=settings.DISTRIBUTION_RUN_EVERY,
            period=IntervalSchedule.SECONDS,
        )

        self.task = PeriodicTask.objects.create(
            name=f"Distribution filter: {self.filter}. Start at {self.start_at}. Finish at {self.finish_at}",
            interval=interval,
            task=settings.DISTRIBUTION_TASK_NAME,
            start_time=self.start_at,
            expires=self.finish_at,
            args=json.dumps([self.pk]),
        )
        self.save(update_fields=["task"])

    def finish_periodic_task(self) -> None:
        self.task.enabled = False
        self.task.save(update_fields=["enabled"])

    def change_time_of_task(self) -> None:
        self.task.start_time = self.start_at
        self.task.expires = self.finish_at
        self.task.save(update_fields=["start_time", "expires"])


def validate_number(value: str) -> None:
    if not value.isdecimal():
        raise ValidationError(f"{value} is not number")

def validate_phone_number(value: str) -> None:
    if re.match(r"^7\d{10}$", value) is None:
        raise ValidationError(f"{value} is not phone number")

class Client(models.Model):
    phone_number = models.CharField(max_length=11, validators=[validate_phone_number])
    operator_code = models.CharField(max_length=11, validators=[validate_number])
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=255)

    class Meta:
        index_together = ["tag", "operator_code"]


class Message(models.Model):
    class StatusChoices(models.IntegerChoices):
        NOT_DELIVERED = 1, "Not delivered"
        DELIVERED = 2, "delivered"

    sent_at = models.DateTimeField(blank=True, null=True)
    status = models.PositiveSmallIntegerField(choices=StatusChoices.choices, default=StatusChoices.NOT_DELIVERED)
    distribution = models.ForeignKey(Distribution, on_delete=models.CASCADE, related_name="messages")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="clients")

    class Meta:
        unique_together = ["distribution", "client"]


@receiver(post_save, sender=Distribution)
def create_or_change_distribution_task(sender, instance: Distribution, created, **kwargs):
    if created:
        logger.info("Distribution with id - %s starting", instance.id)

        instance.start_distribution()
    elif instance.task.enabled:
        logger.info("Distribution with id - %s trying to change time of task", instance.id)

        instance.change_time_of_task()
