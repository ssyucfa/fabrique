from django.contrib import admin
from notifications.models import Distribution, Message, Client

admin.site.register(Distribution, admin.ModelAdmin)
admin.site.register(Message, admin.ModelAdmin)
admin.site.register(Client, admin.ModelAdmin)
