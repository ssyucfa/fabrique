from rest_framework import serializers

from notifications.models import Client, Distribution, Message

DISTRIBUTION_BASE_FIELDS = ("id", "start_at", "text", "filter", "finish_at")

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class DistributionSerializer(serializers.ModelSerializer):
    not_delivered_messages = serializers.IntegerField(read_only=True)
    delivered_messages = serializers.IntegerField(read_only=True)

    class Meta:
        model = Distribution
        fields = DISTRIBUTION_BASE_FIELDS + ("not_delivered_messages", "delivered_messages")


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ("client", "status", "sent_at")


class DistributionDetailSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True)
    class Meta:
        model = Distribution
        fields = DISTRIBUTION_BASE_FIELDS + ("messages", )
