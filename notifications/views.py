from django.db.models import Count, Q
from rest_framework import mixins
from rest_framework.serializers import BaseSerializer
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from notifications.models import Client, Message
from notifications.serializers import ClientSerializer, Distribution, DistributionSerializer, \
    DistributionDetailSerializer


class ClientViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    GenericViewSet
):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class DistributionViewSet(ModelViewSet):
    queryset = Distribution.objects.annotate(
        not_delivered_messages=Count("messages", filter=Q(messages__status=Message.StatusChoices.NOT_DELIVERED)),
        delivered_messages=Count("messages", filter=Q(messages__status=Message.StatusChoices.DELIVERED)),
    )

    def get_serializer_class(self) -> type[BaseSerializer]:
        if self.action in ("list", "create"):
            return DistributionSerializer
        return DistributionDetailSerializer
