import concurrent.futures
import logging

from notifications.models import Message, Distribution
from notifications.provider import NotificationProvider

logger = logging.getLogger("notifications.services")


def distribute_tokens(distribution_id: int) -> None:
    base_messages = Message.objects.filter(distribution_id=distribution_id, status=Message.StatusChoices.NOT_DELIVERED)

    messages_count = base_messages.count()
    messages = base_messages.select_related("distribution", "client")

    logger.info("Found %s messages for distribution with id - %s", messages_count, distribution_id)

    provider = NotificationProvider()

    to_update = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=60) as executor:
        future_to_message = {
            executor.submit(provider.send_message, message.client, message.distribution.text): message
            for message in messages
        }
        for future in concurrent.futures.as_completed(future_to_message):
            message = future_to_message[future]
            try:
                future.result()

                logger.info("Message was delivered for client with id - %s", message.client.id)

                to_update.append(message.id)
            except Exception as e:  # noqa
                logger.error("Failed to send message for %s - %s", message.id, e)

    logger.info("%s messages delivered", len(to_update))

    Message.objects.filter(pk__in=to_update).update(status=Message.StatusChoices.DELIVERED)

    if messages_count != len(to_update):
        logger.warning("Not all messages delivered")
        return

    if not messages:
        distribution = Distribution.objects.get(id=distribution_id)
    else:
        distribution = messages[0].distribution

    distribution.finish_periodic_task()

    logger.info("Task finished for distribution with id - %s", distribution_id)


