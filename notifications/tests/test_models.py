from datetime import timedelta

from django.test import TestCase
from django.utils import timezone
from model_bakery import baker

from notifications.models import Distribution, Client


class DistributionTestCase(TestCase):
    def test_start_distribution(self):
        start_at = timezone.now()
        finish_at = timezone.now() + timedelta(days=1)

        distribution = baker.make(
            Distribution,
            start_at=start_at,
            text="text",
            filter="tag",
            finish_at=finish_at,
        )

        distribution.refresh_from_db()

        self.assertIsNotNone(distribution.task)
        self.assertEqual(distribution.task.start_time, start_at)
        self.assertEqual(distribution.task.expires, finish_at)
        self.assertTrue(distribution.task.enabled)

    def test_filtered_clients_by_tag_or_operator_code(self):
        baker.make(Client, tag="tag")
        baker.make(Client, operator_code="434")

        distribution = Distribution(
            start_at = timezone.now(),
            text = "text",
            filter = "tag",
            finish_at = timezone.now() + timedelta(days=1),
        )
        self.assertEqual(len(distribution.filtered_clients_by_tag_or_operator_code), 1)

        distribution.filter = "434"
        self.assertEqual(len(distribution.filtered_clients_by_tag_or_operator_code), 1)

        distribution.filter = "fadsfasd"
        self.assertEqual(len(distribution.filtered_clients_by_tag_or_operator_code), 0)

    def test_finish_periodic_task(self):
        distribution = baker.make(
            Distribution,
            start_at=timezone.now(),
            text="text",
            filter="tag",
            finish_at=timezone.now() + timedelta(days=1),
        )

        distribution.finish_periodic_task()
        self.assertFalse(distribution.task.enabled)
