from unittest import mock

from django.test import TestCase
from model_bakery import baker

from notifications.models import Distribution, Client, Message
from notifications.provider import NotificationProvider, NotSentError
from notifications.services import distribute_tokens


class ServicesTestCase(TestCase):
    def test_distribute_tokens(self):
        baker.make(Client, 4, tag="tag")
        baker.make(Client, operator_code="434")

        distribution = baker.make(
            Distribution,
            filter="tag",
        )

        with mock.patch.object(NotificationProvider, "send_message", return_value=None):
            distribute_tokens(distribution.id)

        self.assertEqual(Message.objects.all().count(), 4)
        self.assertEqual(Message.objects.filter(status=Message.StatusChoices.DELIVERED).count(), 4)
        self.assertEqual(Message.objects.filter(status=Message.StatusChoices.NOT_DELIVERED).count(), 0)

    def test_distribute_tokens_error(self):
        baker.make(Client, 4, tag="tag")
        baker.make(Client, operator_code="434")

        distribution = baker.make(
            Distribution,
            filter="tag",
        )

        with mock.patch.object(NotificationProvider, "send_message", side_effect=NotSentError()):
            distribute_tokens(distribution.id)

        self.assertEqual(Message.objects.all().count(), 4)
        self.assertEqual(Message.objects.filter(status=Message.StatusChoices.DELIVERED).count(), 0)
        self.assertEqual(Message.objects.filter(status=Message.StatusChoices.NOT_DELIVERED).count(), 4)