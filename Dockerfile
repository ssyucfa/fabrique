FROM python:3.10 as base

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

COPY pyproject.toml poetry.lock ./

RUN pip install --upgrade pip setuptools==59.6.0 poetry wheel
RUN poetry config virtualenvs.create false
RUN poetry export --without-hashes --no-interaction --no-ansi -f requirements.txt -o requirements.txt
RUN pip install --no-cache-dir -r requirements.txt


FROM python:3.10-slim

RUN apt-get -y update && apt-get install -y --no-install-recommends libpq-dev gettext && rm -rf /var/lib/apt/lists/*

ENV PYTHONUNBUFFERED=1

ENV WORKERS_COUNT=2
ENV THREADS_COUNT=2
ENV PORT=8000

COPY --from=base /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

WORKDIR /app/notifications
COPY entrypoint.sh .
COPY launchpad .

RUN sed -i 's/\r$//g' entrypoint.sh
RUN chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]