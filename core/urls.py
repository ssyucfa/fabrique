from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

application_urlpatterns = [
    path("notifications/", include("notifications.urls"))
]

api_urlpatterns = [
    path("v1/", include(application_urlpatterns))
]

schema_patterns = [
    path(route="schema/", view=SpectacularAPIView.as_view(), name="schema"),
    path(route="", view=SpectacularSwaggerView.as_view(url_name="schema"), name="swagger-ui"),
]

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "api/",
        include(
            [
                *api_urlpatterns,
                path("docs/", include(schema_patterns)),
            ]
        ),
    ),
]
